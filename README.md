# Boilerplate

This is the boiler plate starter pack for all gopacom minisites. 

The theme and plugin are in the other repos, not here.

This readme will be replaced with README-GENERIC.md
The .gitlab-ci-generic.yml will be come .gitlab-ci.yml
