# GOPACOM_MINISITE

This_is_a_gopacom_mini_site


## Theme Customizations

Every project is nearly all built from base, generic, foundation code, and existing repos.

If you wish to customize this project, the best way is to make a child theme.

The child theme needs to be based off of the "project" parent that is mixed in at build time.

Once a child theme is made, then be sure in the gitlab variables for this project to add a new variable:

```WP_THEME```

with a value of the name of the child theme.

Be sure to adjust the .gitignore so that this repo takes in the child theme accordingly.

To get the generic parent theme do this:

```
cd wp-content
mkdir themes
cd themes
bash <(wget -qO- https://gitlab.com/gopacom-minisites/theme/-/raw/master/download.sh)
```

## Plugins

Every project has a base set of plugins that will come in a be configured at deploy time. However, there is a plugins.sh file in the wp-content/.

This is a simple script that will install extra plugins for global, local, staging, and production.

to run this script:

```
sh wp-content/plugins.sh
sh wp-content/plugins.sh local  // very typical to get a local dev up and running correctly.
```

There is in every project a "project" plugin that is brought and activated on the build/deploy.

to get the base project plugin locally do:

```
cd wp-content
mkdir plugins
cd plugins
bash <(wget -qO- https://gitlab.com/gopacom-minisites/plugin/-/raw/master/download.sh)
```

Also, if needed yes, just make a normal plugin in this repo and activate in staging and production accordingly.


## .htaccess

By default we ignore this and on each build a generic .htaccess will be brought in.

https://gitlab.com/gopacom-minisites/ci/-/raw/master/.htaccess

However if this project needs some customizations to .htaccess make a .htaccess here and adjust the .gitignore.

The .htaccess of the project will be concatenated with the generic .htaccess during the build. 

